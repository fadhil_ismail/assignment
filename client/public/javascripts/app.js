angular.module('nodeTodo', ['charts.pie'])

.controller('mainController', function($scope, $http) {

    $scope.formData = {};
    $scope.userData = {};
    $scope.expenseData = {};
    $scope.singelExpense = {};
	$scope.simpleText = {};
	$scope.data = [];
	
    // Get all user
    $http.get('/users')
        .success(function(data) {
            $scope.userData = data;
        })
        .error(function(error) {
            console.log('Error: ' + error);
        });
		
    // Get all expenses
    $http.get('/expenses')
        .success(function(data) {
            $scope.expenseData = data;
			getBar('day');
        })
        .error(function(error) {
            console.log('Error: ' + error);
        });
		
    // Get all expenses from one user
	$scope.getUserExpenses = function(userID) {
        $http.get('/userExpenses/' + userID)
            .success(function(data) {
				if(data){
					$scope.expenseData = data.Expenses;
					for(i=0;i < data.Expenses.length ;i++){
						$scope.expenseData[i].User = { name: data.name };
					}
					$scope.simpleText.currentUser = data.name;
				}else{
					$scope.expenseData = {};
					$scope.simpleText.currentUser = '';
				}
				getBar('day');
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
		
	// Create a new user
    $scope.createUser = function(expenseID) {
        $http.post('/users', $scope.formData)
            .success(function(data) {
                $scope.formData = {};
                $scope.userData = data;
            })
            .error(function(error) {
                console.log('Error: ' + error);
            });
    };
	
	// Create a new expense
    $scope.createExpense = function(expenseID) {
        $http.post('/expenses', $scope.formData)
            .success(function(data) {
                $scope.formData = {};
                $scope.expenseData = data;
                getBar('day');
            })
            .error(function(error) {
                console.log('Error: ' + error);
            });
    };
	
	
	
    // Get an expense
    $scope.getExpense = function(expenseID) {
        $http.get('/expense/' + expenseID)
            .success(function(data) {
                $scope.singelExpense = data;
				$scope.singelExpense.dateexpense = new Date($scope.singelExpense.dateexpense);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
	
    // Edit an expense
    $scope.editExpense = function(expenseID) {
        $http.put('/expense/' + expenseID,$scope.singelExpense)
            .success(function(data) {
                $scope.singelExpense = $scope.singelExpense;
				$scope.singelExpense.dateexpense = new Date($scope.singelExpense.dateexpense);
                $scope.expenseData = data;
                getBar('day');
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
	
    // Delete an expense
    $scope.deleteExpense = function(expenseID) {
        $http.delete('/expense/' + expenseID)
            .success(function(data) {
                $scope.expenseData = data;
                getBar('day');
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };	
	
	// Export PDF
	$scope.exportPDF = function() {
        $http.get('/pdf')
		.success(function(data) {
			window.location.href = data.file;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
    };	
		
	function convertdate(string) {
		var d = new Date(string);
		var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var result = days[parseInt(d.getDay())]+', '+d.getDate()+' '+months[parseInt(d.getMonth(), 10)]+' '+d.getFullYear();
		return result;
	}
	
	$scope.change1 = function(){
		getBar('day');
	};
	
	$scope.change2 = function(){
	   getBar('month');
	};
	
	$scope.change3 = function(){
		getBar('year');
	};
	var monthNames = ["January", "February", "March", "April", "May", "June",
	  "July", "August", "September", "October", "November", "December"
	];

	$scope.simpleText.nowIs = new Date().toDateString();
	$scope.simpleText.currentUser = 'Expenses by All User';
	
	function getBar(type){
		var holder = [];
		var datas = $scope.expenseData;
		if(datas.length){
			datas.forEach(function(element) {
				if(type == 'year'){
					var identifier = element.dateexpense.slice(0, 4);
				} else if(type == 'month'){
					var identifier = monthNames[element.dateexpense.slice(5, 7) - 1];
				} else {
					var identifier = element.dateexpense.slice(8, 10)+' '+monthNames[element.dateexpense.slice(5, 7) - 1];
				}
				
				if (holder[identifier]) {
					holder[identifier].amount += element.amount;
				} else {
					holder[identifier] = {amount: element.amount};
				};
				holder[identifier].dateexpense = element.dateexpense;
			});
			$scope.data = [];
			var i = 0;
			for(var identifier in holder) {
				$scope.data[i] = [identifier,holder[identifier].amount];
				i++;
			}
		}else{
			$scope.data = [];
		}
	}
	 
});


angular.module('charts.pie', [])
.directive('qnPiechart', [
	function() {
		return {
			require: '?ngModel',
			link: function(scope, element, attr, controller) {
				var settings = {
					title: 'Expenses',
					bar: {groupWidth: "10%"},
				};

				var getOptions = function() {
					return angular.extend({ }, settings, scope.$eval(attr.qnPiechart));
				};

				// creates instance of datatable and adds columns from settings
				var getDataTable = function() {
					var columns = scope.$eval(attr.qnColumns);
					var data = new google.visualization.DataTable();
					angular.forEach(columns, function(column) {
						data.addColumn(column.type, column.name);
					});
					return data;
				};

				var init = function() {
					var options = getOptions();
					if (controller) {

						var drawChart = function() {
							var data = getDataTable();
							// set model
							data.addRows(controller.$viewValue);

							// Instantiate and draw our chart, passing in some options.
							var pie = new google.visualization.BarChart(element[0]);
							pie.draw(data, options);
						};

						controller.$render = function() {
							drawChart();
						};
					}

					if (controller) {
						// Force a render to override
						controller.$render();
					}
				};

				// Watch for changes to the directives options
				scope.$watch(getOptions, init, true);
				scope.$watch(getDataTable, init, true);
				
				angular.element(window).on('resize', function(){
					controller.$render();
				});
			}
		};
	}
]);