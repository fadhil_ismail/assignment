var express = require('express')
var router = express.Router()
var routes = {
	index: require('./server/routes/index'),
	user: require('./server/routes/user'),
	expense: require('./server/routes/expense'),
	pdf: require('./server/routes/pdf'),
}

module.exports = function (app) {
	app.get('/', routes.index.home);
	app.get('/users', routes.user.getUsers);
	app.get('/userExpenses/:id', routes.user.getUserExpenses);
	app.post('/users', routes.user.postUsers);
	
	app.get('/expenses', routes.expense.getExpenses);
	app.post('/expenses', routes.expense.postExpenses);
	
	app.get('/expense/:id', routes.expense.getExpense);
	app.put('/expense/:id', routes.expense.putExpense);
	app.delete('/expense/:id', routes.expense.delExpense);
	
	app.get('/pdf', routes.pdf.print);
};