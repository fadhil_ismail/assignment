var models = require('../models/index');

exports.getUsers = function (req, res) {
	models.User.findAll({}).then(function(user) {
      res.json(user);
    });
	//res.render('user', { title: "Create user"});
};

// get all expenses
exports.getUserExpenses = function (req, res) {
	models.User.find({
		where: {
		  id: req.params.id
		},
		include: [{
			model: models.Expense,
			required: true
		}]
	}).then(function(expenses) {
      res.json(expenses);
    });
};

exports.postUsers = function (req, res) {
  models.User.create({
    name: req.body.name,
    email: req.body.email
  }).then(function(user) {
    //res.json(user);
	models.User.findAll({}).then(function(user) {
      res.json(user);
    });
  });
};