var models = require('../models/index');

// get all expenses
exports.getExpenses = function (req, res) {
	models.Expense.findAll({
		include: [{
			model: models.User,
			attributes: ['name']
		}],
		attributes: ['name','amount','dateexpense','id']
	}).then(function(expenses) {
      res.json(expenses);
    });
};

exports.postExpenses = function (req, res) {
  models.Expense.create({
    name: req.body.name,
    amount: req.body.amount,
    dateexpense: req.body.dateexpense,
    UserId: req.body.user_id
  }).then(function(expense) {
    //res.json(expense);
	models.Expense.findAll({}).then(function(expenses) {
      res.json(expenses);
    });
  });
};

exports.getExpense = function (req, res) {
  models.Expense.find({
    where: {
      id: req.params.id
    }
  }).then(function(expense) {
    res.json(expense);
  });
};

exports.putExpense = function (req, res) {
  models.Expense.find({
    where: {
      id: req.params.id
    }
  }).then(function(expense) {
    if(expense){
      expense.updateAttributes({
        name: req.body.name,
		amount: req.body.amount,
		dateexpense: req.body.dateexpense,
		UserId: req.body.user_id
      }).then(function(expense) {
        //res.send(expense);
		models.Expense.findAll({}).then(function(expenses) {
		  res.json(expenses);
		});
      });
    }
  });
};

exports.delExpense = function (req, res) {
  models.Expense.destroy({
    where: {
      id: req.params.id
    }
  }).then(function(expense) {
    //res.json(expense);
	models.Expense.findAll({}).then(function(expenses) {
      res.json(expenses);
    });
  });
};