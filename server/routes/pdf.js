var models = require('../models/index');
var path = require('path');
var phantom  = require('phantom');

exports.print = function (req, res) {
	models.Expense.findAll({}).then(function(expenses) {
	  phantom.create().then(function(ph) {
		ph.createPage().then(function(page) {
			page.setting('userAgent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) /37.0.2062.120 Safari/537.36');
			page.setting('webSecurityEnabled', false);
			page.property('paperSize', {
			  format: 'A4',
			  orientation: 'portrait',
			  margin: '1cm',
			});
			page.property('zoomFactor', 1);
			page.open("http://128.199.102.222:3000").then(function(status) {
				page.render(path.join(__dirname, '../../client/public/data', 'expenses.pdf')).then(function() {
					datas = {'file':'data/expenses.pdf'};
					res.json(datas);
					ph.exit();
				});
			});
		});
	  });
    });
};