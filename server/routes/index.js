
/*
 * GET home page.
 */
var path = require('path');

exports.home = function(req, res, next) {
  res.render(path.join(__dirname, '../../client/views', 'index'));
};


