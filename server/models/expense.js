'use strict';
module.exports = function(sequelize, DataTypes) {
  var Expense = sequelize.define('Expense', {
    name: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    dateexpense: DataTypes.DATE,
    UserId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        Expense.belongsTo(models.User);
      }
    }
  });
  return Expense;
};